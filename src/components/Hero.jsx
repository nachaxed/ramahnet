import React from 'react';
import Typed from 'react-typed';

const Hero = () => {
  return (
    <div className='text-white bg-black'>
      <div className='max-w-[800px] mt-[-96px] w-full h-screen mx-auto text-center flex flex-col justify-center'>
        <p className='text-[#6eb4d9] font-bold p-2'>
          Conecta con el Futuro
        </p>
        <h1 className='md:text-7xl sm:text-6xl text-4xl font-bold md:py-6'>
          Descubre el Poder
        </h1>
        <div className='flex justify-center items-center'>
          <p className='md:text-5xl sm:text-4xl text-xl font-bold py-4'>
            Velocidad para
          </p>
          <Typed
            className='md:text-5xl sm:text-4xl text-xl font-bold md:pl-4 pl-2'
            strings={['Navegar', 'Jugar', 'Trabajar']}
            typeSpeed={120}
            backSpeed={140}
            loop
          />
        </div>
        <p className='md:text-2xl text-xl font-bold text-[#ffffff]'>
          Experimenta una conexión excepcional en Malargüe. Transmite, juega y trabaja sin interrupciones gracias a nuestra tecnología avanzada.
        </p>
        <button className='bg-[#6eb4d9] w-[200px] rounded-md font-medium my-6 mx-auto py-3 text-black'>
          Conoce más sobre nosotros
        </button>
      </div>
    </div>
  );
};

export default Hero;
