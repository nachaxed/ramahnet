import React from 'react';
import {
  FaDribbbleSquare,
  FaFacebookSquare,
  FaGithubSquare,
  FaInstagram,
  FaTwitterSquare,
} from 'react-icons/fa';
import logo from '../assets/logo.png';

const Footer = () => {
  return (
    <div className='bg-black text-gray-400 py-16 px-4'>
      <div className='max-w-7xl mx-auto flex flex-col md:flex-row items-center justify-between gap-8'>
        <div className='flex items-center'>
          <img src={logo} alt='RAMAHNET Logo' className='h-16 mr-4' />
          <div>
            <h1 className='text-2xl font-bold text-[#00df9a]'>Ramahnet</h1>
            
          </div>
        </div>
        <div className='flex space-x-4'>
          <FaFacebookSquare size={30} />
          <FaInstagram size={30} />
          <FaTwitterSquare size={30} />
          <FaGithubSquare size={30} />
          <FaDribbbleSquare size={30} />
        </div>
      </div>
      <div className='max-w-7xl mx-auto grid grid-cols-2 md:grid-cols-3 gap-4 mt-8'>
        <div>
          <h6 className='font-medium text-gray-500'>Soporte</h6>
          <ul className='mt-2'>
            <li className='py-1 text-gray-300'>Contacto</li>
            <li className='py-1 text-gray-300'>FAQ</li>
            <li className='py-1 text-gray-300'>Documentación</li>
            <li className='py-1 text-gray-300'>Foro de Ayuda</li>
          </ul>
        </div>
        <div>
          <h6 className='font-medium text-gray-500'>Empresa</h6>
          <ul className='mt-2'>
            <li className='py-1 text-gray-300'>Sobre Nosotros</li>
            <li className='py-1 text-gray-300'>Clientes</li>
            <li className='py-1 text-gray-300'>Equipo</li>
            <li className='py-1 text-gray-300'>Carreras</li>
          </ul>
        </div>
        <div>
          <h6 className='font-medium text-gray-500'>Legal</h6>
          <ul className='mt-2'>
            <li className='py-1 text-gray-300'>Privacidad</li>
            <li className='py-1 text-gray-300'>Términos y Condiciones</li>
            <li className='py-1 text-gray-300'>Política de Cookies</li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Footer;
