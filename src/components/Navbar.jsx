import React, { useState } from 'react';
import { AiOutlineClose, AiOutlineMenu } from 'react-icons/ai';
import logo from '../assets/logo1.png';

const Navbar = () => {
  const [nav, setNav] = useState(false);

  const handleNav = () => {
    setNav(!nav);
  };

  return (
    <div className='flex justify-between items-center h-24 max-w-[1240px] mx-auto px-4 text-white'>
      <img src={logo} alt='RAMAHNET Logo' className='h-16 mx-auto mb-6' />
      <h1 className='w-full text-3xl font-bold text-white'>RAMAHNET</h1>
      <ul className='hidden md:flex text-white'>
        <li className='p-4'>Inicio</li>
        <li className='p-4'>Empresa</li>
        <li className='p-4'>Recursos</li>
        <li className='p-4'>Acerca de</li>
        <li className='p-4'>Contacto</li>
      </ul>

      <div onClick={handleNav} className='block md:hidden text-white'>
        {nav ? <AiOutlineClose size={20} /> : <AiOutlineMenu size={20} />}
      </div>
      <ul className={nav ? 'fixed left-0 top-0 w-[60%] h-full border-r border-gray-900 bg-gray-800 ease-in-out duration-500' : 'ease-in-out duration-500 fixed left-[-100%]'}>
        <img src={logo} alt='Logo de RAMAHNET' className='h-16 mx-auto mb-6' />
        <li className='p-4 border-b border-gray-600 text-white'>Inicio</li>
        <li className='p-4 border-b border-gray-600 text-white'>Empresa</li>
        <li className='p-4 border-b border-gray-600 text-white'>Recursos</li>
        <li className='p-4 border-b border-gray-600 text-white'>Acerca de</li>
        <li className='p-4 text-white'>Contacto</li>
      </ul>
    </div>
  );
};

export default Navbar;
