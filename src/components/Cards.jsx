import React from 'react';
import triple from '../assets/triple.png';
import doble from '../assets/double.png';
import simple from '../assets/single.png';

const Cards = () => {
  return (
    <div className='w-full py-16 px-4 bg-white'>
      <div className='max-w-[1240px] mx-auto grid md:grid-cols-3 gap-8'>
        <div className='w-full shadow-xl flex flex-col p-4 my-4 rounded-lg hover:scale-105 duration-300'>
          <img className='w-20 mx-auto' src={simple} alt="Estándar" /> 
          <h2 className='text-2xl font-bold text-center py-8'>Estándar</h2>
          <p className='text-center text-4xl font-bold'>Ideal para hogares</p>
          <div className='text-center font-medium'>
            <p className='py-2 border-b mx-8 mt-8'>Internet ilimitado de alta velocidad y baja latencia</p>
            <button className='bg-[#004a80] text-white rounded-md font-medium px-6 py-2 mt-4'>A confirmar</button>
          </div>
        </div>

        <div className='w-full shadow-xl bg-gray-100 flex flex-col p-4 my-4 rounded-lg hover:scale-105 duration-300'>
          <img className='w-20 mx-auto' src={doble} alt="Prioridad" />
          <h2 className='text-2xl font-bold text-center py-8'>Empresas</h2>
          <p className='text-center text-4xl font-bold'>Ideal para negocios y usuarios de alta demanda</p>
          <div className='text-center font-medium'>
            <p className='py-2 border-b mx-8 mt-8'>Datos estándar ilimitados</p>
            <button className='bg-[#004a80] text-white rounded-md font-medium px-6 py-2 mt-4'>A confirmar</button>
          </div>
        </div>

        <div className='w-full shadow-xl flex flex-col p-4 my-4 rounded-lg hover:scale-105 duration-300'>
          <img className='w-20 mx-auto' src={triple} alt="Móvil" />
          <h2 className='text-2xl font-bold text-center py-8'>Móvil</h2>
          <p className='text-center text-4xl font-bold'>Ideal para viajeros, nómades y autocaravanas</p>
          <div className='text-center font-medium'>
            <p className='py-2 border-b mx-8 mt-8'>Datos Móviles ilimitados en tierra</p>
            <button className='bg-[#004a80] text-white rounded-md font-medium px-6 py-2 mt-4'>A confirmar</button>
          </div>
        </div>
      </div>

      <div className='flex justify-center mt-8'>
        <button className='bg-[#6eb4d9] text-black rounded-md font-medium mx-2 px-6 py-3'>Ver todos los planes</button>
        <button className='bg-[#6eb4d9] text-black rounded-md font-medium mx-2 px-6 py-3'>Ordenar ya</button>
      </div>
    </div>
  );
};

export default Cards;
