import React from 'react';

const PlanesPage = () => {
  return (
    <div className='w-full py-16 px-4 bg-white'>
      <h1 className='text-3xl font-bold text-center mb-8'>Planes disponibles</h1>
      <div className='max-w-[1240px] mx-auto grid md:grid-cols-3 gap-8'>
        {/* Detalles de los planes */}
        <div className='w-full shadow-xl flex flex-col p-4 my-4 rounded-lg'>
          <h2 className='text-2xl font-bold text-center py-8'>Estándar</h2>
          <p className='text-center text-4xl font-bold'>€40/MES</p>
          <div className='text-center font-medium'>
            <p className='py-2 border-b mx-8 mt-8'>Internet ilimitado de alta velocidad y baja latencia</p>
            <p className='py-2 border-b mx-8'>Datos estándar ilimitados</p>
          </div>
        </div>
        {/* Otros planes aquí */}
      </div>
    </div>
  );
};

export default PlanesPage;
