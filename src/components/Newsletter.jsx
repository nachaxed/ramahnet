import React, { useState } from 'react';

const Newsletter = () => {
  const [email, setEmail] = useState('');

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(`Email suscrito: ${email}`);

    // Aquí podrías enviar el email a través de una API a tu backend
    // o a un servicio de suscripción como Mailchimp

    // Limpiar el campo de email después de enviar
    setEmail('');
  };

  return (
    <div className='w-full py-16 text-white px-4 bg-black'>
      <div className='max-w-[1240px] mx-auto grid lg:grid-cols-3'>
        <div className='lg:col-span-2 my-4'>
          <h1 className='md:text-4xl sm:text-3xl text-2xl font-bold py-2'>
            Suscríbete para recibir noticias en Malargüe
          </h1>
          <p>
            Mantente actualizado con las últimas noticias, eventos y
            desarrollos en Malargüe.
          </p>
        </div>
        <div className='my-4'>
          <form onSubmit={handleSubmit}>
            <div className='flex flex-col sm:flex-row items-center justify-between w-full'>
              <input
                className='p-3 flex w-full rounded-md text-black my-2 bg-white'
                type='email'
                value={email}
                onChange={handleEmailChange}
                placeholder='Introduce tu correo electrónico'
              />
              <button
                type='submit'
                className='bg-[#6eb4d9] text-black rounded-md font-medium w-[200px] ml-4 my-6 px-6 py-3'
              >
                Suscribirse
              </button>
            </div>
          </form>
          <p>
            Nos preocupamos por la protección de tus datos. Lee nuestra{' '}
            <span className='text-[#6eb4d9]'>Política de Privacidad</span>.
          </p>
        </div>
      </div>
    </div>
  );
};

export default Newsletter;
